---
title: 卷积中的感受野
date: '2022-06-20 14:59:30'
head: []
outline: deep
sidebar: false
prev: false
next: false
---

# 卷积中的感受野

# 感受野的问题

## 1. 感受野的定义

决定卷积神经网络中某个神经元输出结果的像素点在原图区域的大小就被称为这个神经元的感受野。

> One of the basic concepts in deep CNNs is the receptive field, or field of view, of a unit in a certain layer in the network. Unlike in fully connected networks, where the value of each unit depends on the entire input to the network, a unit in convolutional networks only depends on a region of the input. **This region in the input is the receptive field for that unit.**

如下一维数据上，三个卷积层中 kernel size=3, stride=1， 那么最后一层的输出中一个点就对应原一维数据上的7个点。

![image.png](assets/image-20220620150025-hf885mu.png)

## 2. 感受野的计算

从前到后的理论感受野计算公式为：

$$
RF_{i} = RF_{i-1} + (kernel\_size_{i} - 1) * feature\_stride_{i-1}
$$

其中：$RF_i$ 为第 $i$ 层的感受野，$kernel\_size_i$ 为第 $i$ 层的卷积核大小，$feature\_stride_{i-1} = \prod_{j=1}^{i-1} s_j$ ；当 $i=0$ 时为输入层，此时 $RF_0 = 1$, $feature\_stride_0 = 1$  

对于上图kernel size =3，stride=1的三个卷积层来说：

1. **第一层的感受野** ：$RF_1 = 1 + (3-1) * 1 = 3$
2. **第二层的感受野** ：$RF_2 = 3 + (3-1) * 1 = 5$
3. **第三层的感受野** ：$RF_3 = 5 + (3-1) * 1 = 7$

> 从上面也可以看出：3 个 3x3 的卷积堆叠与一个7x7的卷积所达到的感受野相同

如果存在膨胀卷积的话，计算公式如下：

$$
RF_{i} = RF_{i-1} + (kernel\_size_{i} - 1) * feature\_stride_{i-1} * dilation_{i}
$$

## 3. 增大感受野的方法

下面列举了三种增大感受野的方法：

1. 堆叠更深的网络层
2. 下采样
3. 膨胀卷积

> The receptive field size of a unit can be increased in a number of ways.  
> **One option is to stack more layers to make the network deeper** , which increases the receptive field size linearly by theory, as each extra layer increases the receptive field size by the kernel size.  
> **Sub-sampling on the other hand increases the receptive field size multiplicatively** . Modern deep CNN architectures like the VGG networks [18] and Residual Networks [8, 6] use a combination of these techniques.

## 4. 有效感受野

有效感受野针对感受野中的每个点对于神经元输出的贡献是不同的，越靠近中间的感受野的贡献越大， **有效感受野只占理论感受野的一部分** 。

> In particular, we discover that  **not all pixels in a receptive field contribute equally to an output unit’s response** . Intuitively it is easy to see that  **pixels at the center of a receptive field have a much larger impact on an output** .

感受野的影响表现为高斯分布

> Surprisingly, we can prove that in many cases the distribution of impact in a receptive field distributes as a Gaussian.

### 4.1 下采样、膨胀卷积

下采样和膨胀卷积都会增加ERF， 这里说的下采样其实指代的是**通过控制stride实现的。**  
​![image.png](assets/image-20220620150543-j1rxzbk.png)

### 4.2 有效感受野的变化

![image.png](assets/image-20220620150558-ky5fvnt.png)  
有效感受野的变化呈现$\sqrt{n}$曲线增大，有效感受野占实际感受野的大小呈现$\frac{1}{\sqrt{n}}$曲线减小。  
​![image.png](assets/image-20220620150611-eaz63b8.png)

## 参考：

[关于感受野的总结](https://zhuanlan.zhihu.com/p/40267131)  
[论文阅读：Understanding the Effective Receptive Field in Deep Convolutional Neural Networks](https://blog.csdn.net/j879159541/article/details/101211358)  
《Understanding the Effective Receptive Field in Deep Convolutional Neural Networks》  
[目标检测和感受野的总结和想法](https://zhuanlan.zhihu.com/p/108493730)
